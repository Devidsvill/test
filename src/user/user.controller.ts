import { Body, UploadedFile,BadRequestException, UseInterceptors, Controller, Request, Get, Param, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiResponse, ApiTags } from '@nestjs/swagger';
import { UserService } from '../user/user.service';
import { AuthGuard } from '@nestjs/passport';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { UpdateUserDto } from './dto/update-user.dto';
import { UpdateUserImageDto } from './dto/update-user-image.dto';
import { FileInterceptor, FilesInterceptor } from "@nestjs/platform-express";
import { diskStorage } from 'multer';
import { extname } from 'path'

@Controller('user')
export class UserController {

    constructor(private readonly userService: UserService) {
    }

    @ApiResponse({ status: 200 })
    @ApiResponse({ status: 401, description: 'Unauthorized' })
    @ApiResponse({ status: 400, description: 'Bad request' })
    @ApiBearerAuth()
    @UseGuards(JwtAuthGuard)
    @Get('my')
    async myProfile(@Request() req): Promise<any> {
        return await this.userService.getById(req.user.userId);
    }
    
    @ApiResponse({ status: 200 })
    @ApiResponse({ status: 401, description: 'Unauthorized' })
    @ApiResponse({ status: 400, description: 'Bad request' })
    @ApiBearerAuth()
    @UseGuards(JwtAuthGuard)
    @Post('update')
    async update(@Request() req, @Body() updateUserDto: UpdateUserDto ): Promise<any> {
        return await this.userService.update(req.user.userId, updateUserDto);
    }

    @ApiResponse({ status: 201 })
    @ApiResponse({ status: 400, description: 'File cannot be empty' })
    @ApiBearerAuth()
    @UseGuards(JwtAuthGuard)
    @Post('updateImage')
    @ApiConsumes('multipart/form-data')
    @ApiBody({
        schema: {
            type: 'object',
            properties: {
                file: {
                    type: 'string',
                    format: 'binary',
                },
            },
        },
    })
    @UseInterceptors(FileInterceptor('file', {
        storage: diskStorage({
            destination: './public'
            , filename: (req, file, cb) => {
                // Generating a 32 random chars long string
                const randomName = Array(32).fill(null).map(() => (Math.round(Math.random() * 16)).toString(16)).join('')
                //Calling the callback passing the random name generated with the original extension name
                cb(null, `${randomName}${extname(file.originalname)}`)
            }
        })
    }))
    async updateUserImage(@Request() req, @UploadedFile() file): Promise<any> {
        if(file === undefined){
            throw new BadRequestException(400, 'File cannot be empty');
        }
        return await this.userService.updateUserImage(req.user.userId, file);
    }

}
