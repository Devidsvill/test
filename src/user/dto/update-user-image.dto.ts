import { ApiProperty } from '@nestjs/swagger';
import {
    IsEmail,
    IsIn,
    IsOptional,
    IsString,
    MinLength,
} from 'class-validator';

export class UpdateUserImageDto {

    @IsString()
    @ApiProperty()
    readonly imageName: string;

    @IsString()
    @ApiProperty()
    readonly base64Code: string;

}
