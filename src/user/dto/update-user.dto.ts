import { ApiProperty } from '@nestjs/swagger';
import {
    MinLength,
    IsEmail,
    MaxLength,
    IsNotEmpty,
    ValidateNested,
    IsDefined,
    Min,
    IsNotEmptyObject,
    IsObject,
    IsString,
} from 'class-validator';

export class UpdateUserDto {

    @IsString()
    @ApiProperty()
    @IsNotEmpty()
    readonly name: string;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    @IsEmail()
    readonly email: string;

}
