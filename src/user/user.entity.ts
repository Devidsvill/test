import { Entity, Index, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    password: string;

    @Index()
    @Column({unique: true})
    email: string;

    @Column({ default: null})
    image: string;
}