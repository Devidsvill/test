import { Injectable  } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';
import { RegisterDto } from '../../src/auth/dto/register.dto';
import { hash, compare } from 'bcrypt';
import { UserExistException } from '../exceptions/index';
import { UpdateUserDto } from './dto/update-user.dto';
import { UpdateUserImageDto } from './dto/update-user-image.dto';

@Injectable()
export class UserService {

    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>,
    ) {}

    async findByEmail(email: string){
        return await this.userRepository
            .createQueryBuilder('user')
            .where('user.email = :email', { email }).getOne();
    }

    async isExist(email: string) {
        const check = await this.findByEmail(email);
        if (check) return true;
        else return false;
    }

    async register(registerDto: RegisterDto): Promise<any> {
        const check = await this.isExist(registerDto.email);
        if (check === false) {
            if (registerDto.password) {
                registerDto.password = await hash(registerDto.password, 10);
            }
            await this.userRepository.save(registerDto);
        }  else {
           throw new UserExistException();
        }
    }

    async getById(userId: number){
        let user = await this.userRepository.findOne(userId);
        if(user){
            delete user.password;
        }
        return user;
    }
    
    async update(userId: number, updateUserDto: UpdateUserDto){

        let user = await this.userRepository.findOne(userId);
        user.email = updateUserDto.email;
        user.name = updateUserDto.name;
        user = await this.userRepository.save(user);
        delete user.password;
        return user;
        
    }
    
    async updateUserImage(userId: string, file: any){
        let user = await this.userRepository.findOne(userId)
        user.image = file.filename;
        user = await this.userRepository.save(user);
        delete user.password;
        return user;
    }

}
