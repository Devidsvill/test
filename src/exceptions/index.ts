import {
  BadRequestException,
  InternalServerErrorException,
} from '@nestjs/common';

export class UserExistException extends BadRequestException {
  constructor() {
    let message = 'User already exists';
    super({
      code: 400,
      message,
    });
  }
}

export class UserNotExistException extends BadRequestException {
  constructor() {
    super({
      code: 102,
      message: 'User not exist',
    });
  }
}

export class WrongPasswordOrEmail extends BadRequestException {
  constructor() {
    super({
      code: 401,
      message: 'Wrong password or email',
    });
  }
}


