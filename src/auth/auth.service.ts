import { Injectable } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { JwtService } from '@nestjs/jwt';
import { LoginDto } from './dto/login.dto';
import { hash, compare } from 'bcrypt';
import { WrongPasswordOrEmail } from '../exceptions'
@Injectable()
export class AuthService {

    constructor(
        private usersService: UserService,
        private jwtService: JwtService
    ) {}
    
    async validateUser(email: string, pass: string): Promise<any> {
        const user = await this.usersService.findByEmail(email);

        if (user) {
            const isPasswordMatching = await compare(pass, user.password);
            if(isPasswordMatching){
                return user;
            }
        }

        throw new WrongPasswordOrEmail();
    }

    async login(loginDto: LoginDto) {

        let user = await this.validateUser(loginDto.email, loginDto.password);
        delete user.password;
        const payload = {email: user.email, sub: user.id};
        return {
            user: user,
            access_token: this.jwtService.sign(payload),
        };
    }
}
