import { ApiProperty } from '@nestjs/swagger';
import {
    IsEmail,
    IsIn,
    IsOptional,
    IsString,
    MinLength,
} from 'class-validator';

export class RegisterDto {

    @IsString()
    @IsOptional()
    @ApiProperty()
    readonly name: string;

    @IsString()
    @IsEmail()
    @IsOptional()
    @ApiProperty()
    readonly email: string;

    @MinLength(8)
    @IsString()
    @IsOptional()
    @ApiProperty()
    password: string;

}
